<?php

namespace App\Repository;

use App\Entity\Type;
use App\Entity\User;
use App\Entity\UserHasGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use function PHPSTORM_META\type;

/**
 * @method UserHasGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserHasGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserHasGame[]    findAll()
 * @method UserHasGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserHasGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserHasGame::class);
    }

    public function getGamesFromUser(User $user,$ordone) {

        return $this->createQueryBuilder('uhg')
            ->addSelect('g')
            ->addSelect('p')
            ->innerJoin('uhg.Game', 'g')
            ->innerJoin('uhg.Platform', 'p')
            ->where('uhg.user = :user')
            ->orderBy('g.titleGame', $ordone)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
    ;

    }

    public function getGamesFromUserPlatforme(User $user,$nomPlateforme) {

        return $this->createQueryBuilder('uhg')
            ->addSelect('g')
            ->addSelect('p')
            ->innerJoin('uhg.Game', 'g')
            ->innerJoin('uhg.Platform', 'p')
            ->where('uhg.user = :user')
            ->setParameter('user', $user)
            ->andWhere('p.namePlat = :namPlateform')
            ->orderBy('g.titleGame', 'ASC')
            ->setParameter('namPlateform', $nomPlateforme)
            ->getQuery()
            ->getResult()
    ;

    }

    
    public function getGamesFromUserType(User $user,$type) {

        return $this->createQueryBuilder('uhg')
            ->addSelect('g')
            ->addSelect('t')
            ->innerJoin('uhg.Game','g')
            ->innerJoin('g.types','t')
            ->where('uhg.user = :user')
            ->setParameter('user', $user)
            ->andWhere('t.titleType = :type')
            ->setParameter('type', $type)
            ->orderBy('g.titleGame', 'ASC')
            ->getQuery()
            ->getResult()
    ;

    }

    public function getGamesFromUserAll(User $user,$type,$nomPlateforme,$ordone) {

        return $this->createQueryBuilder('uhg')
            ->addSelect('g')
            ->addSelect('t')
            ->addSelect('p')
            ->innerJoin('uhg.Game','g')
            ->innerJoin('uhg.Platform', 'p')
            ->innerJoin('g.types','t')
            ->where('uhg.user = :user')
            ->setParameter('user', $user)
            ->andWhere('t.titleType = :type OR true = :type')
            ->setParameter('type', $type)
            ->andWhere('p.namePlat = :namPlateform OR true = :namPlateform')
            ->setParameter('namPlateform', $nomPlateforme)
            ->orderBy('g.titleGame', $ordone)
            ->getQuery()
            ->getResult()
    ;

    }
   
    

    // /**
    //  * @return UserHasGame[] Returns an array of UserHasGame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserHasGame
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

 
}
