<?php

namespace App\Form;

use App\Entity\UserHasGame;
use App\Entity\Game;
use App\Entity\Platform;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserHasGameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ownedDate')
            ->add('Game', EntityType::class, [
                'class' => Game::class,
                'choice_label' => 'titleGame',
                'multiple' => false,
                'expanded' => true,
              
            ])
            ->add('Platform', EntityType::class, [
                'class' => Platform::class,
                'choice_label' => 'namePlat',
                'multiple' => true,
                'expanded' => true,
              
            ])

          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserHasGame::class,
        ]);
    }
}
