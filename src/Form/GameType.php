<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Platform;
use App\Entity\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleGame')
            ->add('descriptionGame')
            ->add('releasedAt', DateType::class, [
                // renders it as a single text box
                'widget' => 'single_text',
            ])
            ->add('imageGame',FileType::class, [
                'label' => 'Logo du jeux',
                'mapped' => false,
                'required' => false,])

            ->add('nbSellGame')

                ->add('types', EntityType::class, [
                    'class' => Type::class,
                    'choice_label' => 'titleType',
                    'multiple' => true,
                    'expanded' => true,
            
                ])
            ->add('platforms', EntityType::class, [
                'class' => Platform::class,
                'choice_label' => 'namePlat',
                'multiple' => true,
                'expanded' => true,
              
            ])


           
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
