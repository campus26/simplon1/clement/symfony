<?php

namespace App\Entity;

use App\Repository\PlatformRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlatformRepository::class)
 */
class Platform
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $namePlat;

    /**
     * @ORM\Column(type="text")
     */
    private $descriptionPlat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logoPlat;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, mappedBy="platforms")
     */
    private $Game;

    /**
     * @ORM\ManyToMany(targetEntity=UserHasGame::class, mappedBy="Platform")
     */
    private $userHasGames;

    public function __construct()
    {
        $this->Game = new ArrayCollection();
        $this->userHasGames = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNamePlat(): ?string
    {
        return $this->namePlat;
    }

    public function setNamePlat(string $namePlat): self
    {
        $this->namePlat = $namePlat;

        return $this;
    }

    public function getDescriptionPlat(): ?string
    {
        return $this->descriptionPlat;
    }

    public function setDescriptionPlat(string $descriptionPlat): self
    {
        $this->descriptionPlat = $descriptionPlat;

        return $this;
    }

    public function getLogoPlat(): ?string
    {
        return $this->logoPlat;
    }

    public function setLogoPlat(?string $logoPlat): self
    {
        $this->logoPlat = $logoPlat;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGame(): Collection
    {
        return $this->Game;
    }

    public function addGame(Game $game): self
    {
        if (!$this->Game->contains($game)) {
            $this->Game[] = $game;
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        $this->Game->removeElement($game);

        return $this;
    }

    /**
     * @return Collection|UserHasGame[]
     */
    public function getUserHasGames(): Collection
    {
        return $this->userHasGames;
    }

    public function addUserHasGame(UserHasGame $userHasGame): self
    {
        if (!$this->userHasGames->contains($userHasGame)) {
            $this->userHasGames[] = $userHasGame;
            $userHasGame->addPlatform($this);
        }

        return $this;
    }

    public function removeUserHasGame(UserHasGame $userHasGame): self
    {
        if ($this->userHasGames->removeElement($userHasGame)) {
            $userHasGame->removePlatform($this);
        }

        return $this;
    }
}
