<?php

namespace App\Entity;

use App\Repository\UserHasGameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserHasGameRepository::class)
 */
class UserHasGame
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $ownedDate;



    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="userHasGames")
     */
    private $Game;

    /**
     * @ORM\ManyToMany(targetEntity=Platform::class, inversedBy="userHasGames")
     */
    private $Platform;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="UserHasGame")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;




    public function __construct()
    {
        $this->Platform = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwnedDate(): ?\DateTimeInterface
    {
        return $this->ownedDate;
    }

    public function setOwnedDate(\DateTimeInterface $ownedDate): self
    {
        $this->ownedDate = $ownedDate;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->Game;
    }

    public function setGame(?Game $Game): self
    {
        $this->Game = $Game;

        return $this;
    }

    /**
     * @return Collection|Platform[]
     */
    public function getPlatform(): Collection
    {
        return $this->Platform;
    }

    public function addPlatform(Platform $platform): self
    {
        if (!$this->Platform->contains($platform)) {
            $this->Platform[] = $platform;
        }

        return $this;
    }

    public function removePlatform(Platform $platform): self
    {
        $this->Platform->removeElement($platform);

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
