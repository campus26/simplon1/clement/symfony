<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ObjectManager;

class LuckyController extends AbstractController
{
  

    /**
     * @Route("/lucky", name="lucky")  //CECI EST DU CODE !!!! C'est une annotation PHP
     */
    public function numberAction()
    {
        // génération d'un nombre aléatoire
        $number = mt_rand(0, 100);
  

        //ici on va chercher le template et on lui transmet la variable
        return $this->render('AdminTemplate/login.html.twig', [
            // pour fournir des variables au template
            // a gauche, le nom qui sera utilisé dans le template
            // a droite, la valeur
            'number' => $number,
            'controller_name' => 'LuckyController'
            
       ]);
    }

}
