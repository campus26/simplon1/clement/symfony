<?php

namespace App\Controller;

use App\Entity\UserHasGame;
use App\Form\UserHasGameType;
use App\Repository\PlatformRepository;
use App\Repository\TypeRepository;
use App\Repository\UserHasGameRepository;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;

/**
 * @Route("/userGame")
 */
class UserHasGameController extends AbstractController
{
    /**
     * @Route("/", name="user_has_game_index", methods={"GET"})
     */
    public function index(Request $request, UserHasGameRepository $userHasGameRepository, PlatformRepository $platformRepository, TypeRepository $typeRepository): Response
    {
        $user = $this->getUser();


        if ($request->get('ordone') !== null) {
            $ordone = $request->get('ordone');
        } else {
            $ordone = 'ASC';
        }

 
     if ($request->query->keys() !== []) {

            if ($request->get('platforme') !== null) {
                $maPlatforme = $request->get('platforme');
            } else {
                $maPlatforme = true;
            }

            if ($request->get('type') !== null) {
                $monType = $request->get('type');
            } else {
                $monType = true;
            }

            $userGames = $userHasGameRepository->getGamesFromUserAll($user, $monType, $maPlatforme,$ordone);
        } else {
            $userGames = $userHasGameRepository->getGamesFromUser($user,$ordone);
    }



        $platformes = $platformRepository->findAll();
        $types = $typeRepository->findAll();


        return $this->render('user_has_game/index.html.twig', [
            'user_has_games' => $userGames,
            'plat' =>  $platformes,
            'types' =>  $types,

        ]);
    }

    /**
     * @Route("/new", name="user_has_game_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $userHasGame = new UserHasGame();
        $form = $this->createForm(UserHasGameType::class, $userHasGame);
        $form->handleRequest($request);



        $userHasGame->setUser($this->getUser());


        if ($form->isSubmitted() && $form->isValid()) {


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userHasGame);
            $entityManager->flush();

            return $this->redirectToRoute('user_has_game_index');
        }

        return $this->render('user_has_game/new.html.twig', [
            'user_has_game' => $userHasGame,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/{id}", name="user_has_game_show", methods={"GET"})
     */
    public function show(UserHasGame $userHasGame): Response
    {
        return $this->render('user_has_game/show.html.twig', [
            'user_has_game' => $userHasGame,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_has_game_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserHasGame $userHasGame): Response
    {
        $form = $this->createForm(UserHasGameType::class, $userHasGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_has_game_index');
        }

        return $this->render('user_has_game/edit.html.twig', [
            'user_has_game' => $userHasGame,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_has_game_delete", methods={"POST"})
     */
    public function delete(Request $request, UserHasGame $userHasGame): Response
    {
        if ($this->isCsrfTokenValid('delete' . $userHasGame->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userHasGame);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_has_game_index');
    }
}
