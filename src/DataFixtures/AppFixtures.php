<?php

namespace App\DataFixtures;


use App\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        
      
            $product = new Type();
            $product->setTitleType('Aventure');
            $product->setDescriptionType('tout les jeux d\'avenure');
            $manager->persist($product);

            $product = new Type();
            $product->setTitleType('Plateforme');
            $product->setDescriptionType('Jeux de plateforme');
            $manager->persist($product);


            $product = new Type();
            $product->setTitleType('Simulation');
            $product->setDescriptionType('Les jeux de simulation');
            $manager->persist($product);

            $product = new Type();
            $product->setTitleType('MOBA');
            $product->setDescriptionType(' Arène de bataille en ligne multijoueur ');
            $manager->persist($product);
      

        $manager->flush();
    }
      
    
}
